import pytest

import mangoes

ENCODING = "UTF-8"

# Raw text, no annotation
RAW_SOURCE = """Beautiful is better than ugly
             Explicit is better than implicit
             Simple is better than complex
             Complex is better than complicated
             Flat is better than nested
             Sparse is better than dense""".split("\n")

# Annotated text
ANNOTATED_SOURCE = """Beautiful/JJ/beautiful is/VBZ/be better/JJR/better than/IN/than ugly/JJ/ugly
                      Explicit/NNP/Explicit is/VBZ/be better/JJR/better than/IN/than implicit/JJ/implicit
                      Simple/NN/simple is/VBZ/be better/JJR/better than/IN/than complex/JJ/complex
                      Complex/NN/complex is/VBZ/be better/JJR/better than/IN/than complicated/VBN/complicate
                      Flat/NNP/Flat is/VBZ/be better/JJR/better than/IN/than nested/JJ/nested
                      Sparse/NNP/Sparse is/VBZ/be better/JJR/better than/IN/than dense/JJ/dense""".split("\n")

# 6 sentences, 30 words
# is, better, than : 6 (-> 18)
# beautiful, ugly, explicit, implicit, simple, complicated, flat, nested, sparse, dense : 1 (-> 10)
# complex : 1 + 1 with capital -> 2
# 14 words if lower cased / 15 if case sensitive


@pytest.fixture()
def DummyAnnotatedReader(annotated_reader_expected):
    class DummyAnnotatedReader:
        def __init__(self, *args, **kwargs):
            pass

        def sentences(self):
            yield from [sentence for sentence in annotated_reader_expected]

    return DummyAnnotatedReader

@pytest.fixture()
def DummyAnnotatedReaderLower(annotated_reader_expected):
    class DummyAnnotatedReader:
        def __init__(self, *args, **kwargs):
            pass

        def sentences(self):
            yield from [[token.lower() for token in sentence] for sentence in annotated_reader_expected]

    return DummyAnnotatedReader


@pytest.fixture()
def dummy_raw_corpus(raw_source_string):
    class DummyRawCorpus:
        def __init__(self, *args, **kwargs):
            self.source = raw_source_string
            self.nb_sentences = 6
            self.size = 30
            self.words_count = {'is': 6, 'better': 6, 'than': 6,
                                'beautiful': 1, 'ugly': 1, 'explicit': 1, 'implicit': 1,
                                'simple': 1, 'complex': 2, 'complicated': 1,
                                'flat': 1, 'nested': 1, 'sparse': 1, 'dense': 1}
            self.params={"name": "dummy"}

        def __iter__(self):
            for sentence in self.source:
                yield sentence.lower().split()
    return DummyRawCorpus(None)


@pytest.fixture()
def dummy_annotated_corpus(DummyAnnotatedReaderLower):
    return mangoes.Corpus(None, reader=DummyAnnotatedReaderLower)


@pytest.fixture()
def annotated_reader_expected():
    expected = ['word:Beautiful/pos:JJ/lemma:beautiful word:is/pos:VBZ/lemma:be word:better/pos:JJR/lemma:better '
                'word:than/pos:IN/lemma:than word:ugly/pos:JJ/lemma:ugly',

                'word:Explicit/pos:NNP/lemma:Explicit word:is/pos:VBZ/lemma:be word:better/pos:JJR/lemma:better '
                'word:than/pos:IN/lemma:than word:implicit/pos:JJ/lemma:implicit',

                'word:Simple/pos:NN/lemma:simple word:is/pos:VBZ/lemma:be word:better/pos:JJR/lemma:better '
                'word:than/pos:IN/lemma:than word:complex/pos:JJ/lemma:complex',

                'word:Complex/pos:NN/lemma:complex word:is/pos:VBZ/lemma:be word:better/pos:JJR/lemma:better '
                'word:than/pos:IN/lemma:than word:complicated/pos:VBN/lemma:complicate',

                'word:Flat/pos:NNP/lemma:Flat word:is/pos:VBZ/lemma:be word:better/pos:JJR/lemma:better '
                'word:than/pos:IN/lemma:than word:nested/pos:JJ/lemma:nested',

                'word:Sparse/pos:NNP/lemma:Sparse word:is/pos:VBZ/lemma:be word:better/pos:JJR/lemma:better '
                'word:than/pos:IN/lemma:than word:dense/pos:JJ/lemma:dense']

    return [sentence.split() for sentence in expected]

@pytest.fixture()
def raw_reader_expected(raw_source_string):
    return [sentence.split() for sentence in raw_source_string]

@pytest.fixture()
def raw_reader_lower_expected(raw_source_string):
    return [sentence.lower().split() for sentence in raw_source_string]

# complex : 1 with pos NN + 1 with pos JJ

# From RAW_SOURCE, generate different inputs
@pytest.fixture()
def raw_source_string():
    return RAW_SOURCE


@pytest.fixture()
def raw_source_file(tmpdir_factory):
    source_file = tmpdir_factory.mktemp('data').join('RAW_SOURCE.txt')
    source_file.write_text("\n".join(RAW_SOURCE), encoding=ENCODING)
    return str(source_file)


@pytest.fixture()
def raw_source_dir(tmpdir_factory):
    source_dir = tmpdir_factory.mktemp('data')
    for i, s in enumerate(RAW_SOURCE):
        f = source_dir.join('source_{}.txt'.format(i))
        f.write_text(s, encoding=ENCODING)
    return str(source_dir)


# From ANNOTATED_SOURCE, generate 3 formats : brown, xml or conll
@pytest.fixture()
def brown_source_string():
    return ANNOTATED_SOURCE


@pytest.fixture()
def brown_source_file(tmpdir_factory):
    source_file = tmpdir_factory.mktemp('data').join('BROWN_SOURCE.txt')
    source_file.write_text("\n".join(ANNOTATED_SOURCE), encoding=ENCODING)
    return str(source_file)


@pytest.fixture()
def brown_source_dir(tmpdir_factory):
    source_dir = tmpdir_factory.mktemp('data')
    for i, s in enumerate(ANNOTATED_SOURCE):
        f = source_dir.join('source_{}.txt'.format(i))
        f.write_text(s, encoding=ENCODING)
    return str(source_dir)


@pytest.fixture()
def xml_source_string():
    xml_string = "<root><document><sentences>"
    for sentence in ANNOTATED_SOURCE:
        xml_string += "<sentence><tokens>"
        for token in sentence.split():
            word, pos, lemma = token.split("/")
            xml_string += "<token>"
            xml_string += "<word>" + word + "</word>"
            xml_string += "<pos>" + pos + "</pos>"
            xml_string += "<lemma>" + lemma + "</lemma>"
            xml_string += "</token>"
        xml_string += "</tokens></sentence>"
    xml_string += "</sentences></document></root>"

    return xml_string


@pytest.fixture()
def xml_source_file(tmpdir_factory):
    source_file = tmpdir_factory.mktemp('data').join('RAW_SOURCE.xml')
    xml_string = "<root><document><sentences>"
    for sentence in ANNOTATED_SOURCE:
        xml_string += "<sentence><tokens>"
        for token in sentence.split():
            word, pos, lemma = token.split("/")
            xml_string += "<token>"
            xml_string += "<word>" + word + "</word>"
            xml_string += "<pos>" + pos + "</pos>"
            xml_string += "<lemma>" + lemma + "</lemma>"
            xml_string += "</token>"
        xml_string += "</tokens></sentence>"
    xml_string += "</sentences></document></root>"
    source_file.write_text(xml_string, encoding="utf-8")
    return str(source_file)


@pytest.fixture()
def xml_source_dir(tmpdir_factory):
    source_dir = tmpdir_factory.mktemp('data')
    for i, sentence in enumerate(ANNOTATED_SOURCE):
        source_file = source_dir.join('source_{}.xml'.format(i))
        xml_string = "<root><document><sentences><sentence><tokens>"
        for token in sentence.split():
            word, pos, lemma = token.split("/")
            xml_string += "<token>"
            xml_string += "<word>" + word + "</word>"
            xml_string += "<pos>" + pos + "</pos>"
            xml_string += "<lemma>" + lemma + "</lemma>"
            xml_string += "</token>"
        xml_string += "</tokens></sentence></sentences></document></root>"
        source_file.write_text(xml_string, encoding="utf-8")
    return str(source_dir)


@pytest.fixture()
def conll_source_string():
    conll_str = []
    for sentence in ANNOTATED_SOURCE:
        for i, token in enumerate(sentence.split()):
            word, pos, lemma = token.split("/")
            conll_str.append("{} {} {} {} _ _ _".format(i + 1, word, lemma, pos))
        conll_str.append("")
    del(conll_str[-1])
    return conll_str


@pytest.fixture()
def conll_source_file(tmpdir_factory, conll_source_string):
    source_file = tmpdir_factory.mktemp('data').join('source.conll')
    source_file.write(conll_source_string)
    return str(source_file)


@pytest.fixture()
def conll_source_dir(tmpdir_factory):
    source_dir = tmpdir_factory.mktemp('data')
    for i, sentence in enumerate(ANNOTATED_SOURCE):
        source_file = source_dir.join('source_{}.xml'.format(i))
        for j, token in enumerate(sentence.split()):
            word, pos, lemma = token.split("/")
            conll_string = "{}\t{}\t{}\t{}\t_\t_\t0".format(j, word, lemma, pos)
            source_file.write_text(conll_string, encoding="utf-8")
    return str(source_dir)


@pytest.fixture
def save_temp_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('tmp')