# -*- coding: utf-8 -*-
import mangoes.utils.reader
import pytest


# Raw text, no annotation
@pytest.mark.unittest
def test_raw_reader_from_list_of_strings(raw_source_string, raw_reader_expected):
    reader = mangoes.utils.reader.TextSentenceGenerator(raw_source_string)
    for i, sentence in enumerate(reader.sentences()):
        assert raw_reader_expected[i] == sentence
    # TODO : mock mangoes.utils.io.get_reader to make it really unit (idem for the other ones)


@pytest.mark.integration
def test_raw_reader_from_one_file(raw_source_file, raw_reader_expected):
    reader = mangoes.utils.reader.TextSentenceGenerator(raw_source_file)
    for i, sentence in enumerate(reader.sentences()):
        assert raw_reader_expected[i] == sentence


@pytest.mark.integration
def test_raw_reader_from_one_dir(raw_source_dir, raw_reader_expected):
    reader = mangoes.utils.reader.TextSentenceGenerator(raw_source_dir)
    for i, sentence in enumerate(reader.sentences()):
        assert raw_reader_expected[i] == sentence


# Annotated text : 3 formats : brown, xml or conll
# ### XML
@pytest.mark.unittest
def test_xml_reader_from_xml_string(xml_source_string, annotated_reader_expected):
    reader = mangoes.utils.reader.XmlSentenceGenerator(xml_source_string)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_xml_reader_from_xml_file(xml_source_file, annotated_reader_expected):
    reader = mangoes.utils.reader.XmlSentenceGenerator(xml_source_file)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_xml_reader_from_xml_dir(xml_source_dir, annotated_reader_expected):
    reader = mangoes.utils.reader.XmlSentenceGenerator(xml_source_dir)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


# ### BROWN
@pytest.mark.unittest
def test_brown_reader_from_brown_string(brown_source_string, annotated_reader_expected):
    reader = mangoes.utils.reader.BrownSentenceGenerator(brown_source_string)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_brown_reader_from_brown_file(brown_source_file, annotated_reader_expected):
    reader = mangoes.utils.reader.BrownSentenceGenerator(brown_source_file)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_brown_reader_from_brown_dir(brown_source_dir, annotated_reader_expected):
    reader = mangoes.utils.reader.BrownSentenceGenerator(brown_source_dir)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


# ### CONLL
@pytest.mark.unittest
def test_conll_reader_from_conll_string(conll_source_string, annotated_reader_expected):
    reader = mangoes.utils.reader.ConllSentenceGenerator(conll_source_string)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_conll_reader_from_conll_file(conll_source_file, annotated_reader_expected):
    reader = mangoes.utils.reader.ConllSentenceGenerator(conll_source_file)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_conll_reader_from_conll_dir(conll_source_dir, annotated_reader_expected):
    reader = mangoes.utils.reader.ConllSentenceGenerator(conll_source_dir)
    for i, sentence in enumerate(reader.sentences()):
        assert annotated_reader_expected[i] == sentence


# ###################
# Filter words' attributes

# only one attribute
expected_word = [['word:Beautiful', 'word:is', 'word:better', 'word:than', 'word:ugly'],
                 ['word:Explicit', 'word:is', 'word:better', 'word:than', 'word:implicit'],
                 ['word:Simple', 'word:is', 'word:better', 'word:than', 'word:complex'],
                 ['word:Complex', 'word:is', 'word:better', 'word:than', 'word:complicated'],
                 ['word:Flat', 'word:is', 'word:better', 'word:than', 'word:nested'],
                 ['word:Sparse', 'word:is', 'word:better', 'word:than', 'word:dense']]


@pytest.mark.unittest
def test_xml_reader_filter_word_attribute(xml_source_string):
    reader = mangoes.utils.reader.XmlSentenceGenerator(xml_source_string, filter_attributes="word")
    for i, sentence in enumerate(reader.sentences()):
        assert expected_word[i] == sentence


@pytest.mark.unittest
def test_brown_filter_word_attribute(brown_source_string):
    reader = mangoes.utils.reader.BrownSentenceGenerator(brown_source_string, filter_attributes="word")
    for i, sentence in enumerate(reader.sentences()):
        assert expected_word[i] == sentence


@pytest.mark.unittest
def test_conll_filter_word_attribute(conll_source_string):
    reader = mangoes.utils.reader.ConllSentenceGenerator(conll_source_string, filter_attributes="word")
    for i, sentence in enumerate(reader.sentences()):
        assert expected_word[i] == sentence


# Two attributes
expected_pos_lemma = [
    ['pos:JJ/lemma:beautiful', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than', 'pos:JJ/lemma:ugly'],
    ['pos:NNP/lemma:Explicit', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
     'pos:JJ/lemma:implicit'],
    ['pos:NN/lemma:simple', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than', 'pos:JJ/lemma:complex'],
    ['pos:NN/lemma:complex', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
     'pos:VBN/lemma:complicate'],
    ['pos:NNP/lemma:Flat', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than', 'pos:JJ/lemma:nested'],
    ['pos:NNP/lemma:Sparse', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than', 'pos:JJ/lemma:dense']]


@pytest.mark.unittest
def test_xml_reader_filter_lemma_and_pos_attributes(xml_source_string):
    reader = mangoes.utils.reader.XmlSentenceGenerator(xml_source_string, filter_attributes=("lemma", "pos"))
    for i, sentence in enumerate(reader.sentences()):
        assert expected_pos_lemma[i] == sentence


@pytest.mark.unittest
def test_brown_filter_lemma_and_pos_attributes(brown_source_string):
    reader = mangoes.utils.reader.BrownSentenceGenerator(brown_source_string, filter_attributes=("lemma", "pos"))
    for i, sentence in enumerate(reader.sentences()):
        assert expected_pos_lemma[i] == sentence


@pytest.mark.unittest
def test_conll_filter_lemma_and_pos_attributes(conll_source_string):
    reader = mangoes.utils.reader.ConllSentenceGenerator(conll_source_string, filter_attributes=("lemma", "pos"))
    for i, sentence in enumerate(reader.sentences()):
        assert expected_pos_lemma[i] == sentence
