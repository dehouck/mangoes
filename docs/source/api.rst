API
==============


Main modules
-------------

.. toctree::
    :maxdepth: 2

    api/mangoes.base
    api/mangoes.corpus
    api/mangoes.vocabulary
    api/mangoes.counting
    api/mangoes.evaluate
    api/mangoes.visualize
    api/mangoes.context
    api/mangoes.reduction
    api/mangoes.weighting
    api/mangoes.dataset

    api/mangoes.generator.word2vec


Utility modules
---------------

.. toctree::

    api/mangoes.constants
    api/mangoes.utils.arrays
    api/mangoes.utils.decorators
    api/mangoes.utils.exceptions
    api/mangoes.utils.io
    api/mangoes.utils.metrics
    api/mangoes.utils.multiproc
    api/mangoes.utils.options
    api/mangoes.utils.persist
    api/mangoes.utils.reader




