# -*- coding: utf-8 -*-
"""
This demo script creates various embeddings from a tokenized corpus using methods based on a matrix build by counting
co-occurrences of words in a corpus :
- the first method just uses the rows of this co-occurrence counts matrix as vectors to represent words
- the second method apply a PPMI on this matrix
- the third method applies a PPMI and reduces the matrix to 50 dimensions with PCA
- the fourth method applies a PPMI and reduces the matrix to 50 dimensions with SVD

We use as a corpus a sample of 750K words from wikipedia articles, from which we extract a vocabulary of 1500 words.
This vocabulary is used both as target words (represented as vectors) and as contexts.
The cooccurrence matrix is generated from the corpus considering a symmetric windows of 2 words around the target word.

"""
import logging
import os.path

import matplotlib.pyplot as plt
import pandas as pd

import mangoes
import mangoes.visualize

#########################
# HYPERPARAMETERS

VOCABULARY_SIZE = 1500
WINDOW_SIZE = 2  # symmetric window of 2 words before and 2 words after the target word
OUTPUT_PATH = os.path.join(os.path.dirname(__file__), "output")


def main():
    logging.basicConfig(level=logging.INFO)

    if not os.path.exists(OUTPUT_PATH):
        os.makedirs(OUTPUT_PATH)

    # Corpus
    corpus = get_demo_corpus()

    # Vocabulary
    vocabulary = get_vocabulary(corpus)

    ############################
    # COUNTING THE COOCCURRENCES
    logging.info("Counting co-occurrences ...")
    cooccurrence_path = os.path.join(OUTPUT_PATH,
                                     "cooccurrence_{}words_win{}".format(VOCABULARY_SIZE, WINDOW_SIZE))

    # Using the previously created vocabulary as the list of words to represent and as the contexts,
    # we count the co-occurrences and create a sparse matrix with these counts
    # Then we save the resulting object (vocabulary + matrix) : it could be loaded the next time

    window = mangoes.context.Window(symmetric=True, window_half_size=WINDOW_SIZE)
    cooccurrence_matrix = mangoes.counting.count_cooccurrence(corpus, vocabulary, vocabulary,
                                                              context_definition=window)
    cooccurrence_matrix.save(cooccurrence_path)
    logging.info("Done. Cooccurrence counts and vocabulary saved in {}\n".format(cooccurrence_path))

    # You can also load a previously saved matrix by commenting the lines above and uncommenting the following
    cooccurrence_matrix = mangoes.CountBasedRepresentation.load(cooccurrence_path)

    ############################
    # FROM COUNTING TO EMBEDDINGS
    logging.info("Creating the representation ...")
    embedding_path = os.path.join(OUTPUT_PATH,
                                  "embeddings/ppmi_svd_{}words_win{}".format(VOCABULARY_SIZE, WINDOW_SIZE))

    # From the cooccurrence counts, create vectors representing words in a mangoes.Embeddings objects,
    # applying ppmi and svd
    embedding = mangoes.create_representation(cooccurrence_matrix,
                                              weighting=mangoes.weighting.PPMI(),
                                              reduction=mangoes.reduction.SVD(dimensions=50, weight=0.5))
    embedding.save(embedding_path)
    logging.info("Done. Matrix and vocabulary saved in {}\n".format(embedding_path))

    # You can also load a previously saved embedding by commenting the lines above and uncommenting the following
    embedding = mangoes.Embeddings.load(embedding_path)

    ##########################
    # EVALUATE THE EMBEDDINGS

    # 1. Closest words
    # you can have a list of the n closest words of a given word according to an embedding
    # To do so, use the get_closest_words() method
    # Here we will display the 3 closest words of 10 words of our vocabulary
    print("\n>> Closest words :")

    result = {word: pd.Series([w for w, _ in embedding.get_closest_words(word, nb=3)], index=[1, 2, 3])
              for word in cooccurrence_matrix.words[:10]}
    print(pd.DataFrame(result).transpose())

    # 2. Analogy task
    # the function mangoes.evaluate.analogy evaluates a representation according to
    # available analogy datasets (Google, MSR, defined in mangoes.dataset)
    print("\n>> Analogy :")
    print(mangoes.evaluate.analogy(embedding).detail)

    # 3. Similarity task
    # the function mangoes.evaluate.similarity evaluates a representation according to
    # available word similarity datasets (ws353, rareword, ... defined in mangoes.dataset)
    print("\n>> Similarity :")
    print(mangoes.evaluate.similarity(embedding).detail)

    # 4. Outlier detection
    # the function mangoes.evaluate.outlier_detection evaluates a representation according to
    # available outlier detection datasets (8-8-8, WikiSem500 defined in mangoes.dataset)
    print("\n>> Outlier detection :")
    print(mangoes.evaluate.outlier_detection(embedding).detail)

    ##########################
    # VISUALIZE

    plt.figure()

    # 1. distances between the words
    ax = plt.subplot(221, projection='polar')
    mangoes.visualize.plot_distances(embedding, ax)

    # 2. isotropy
    ax = plt.subplot(222)
    mangoes.visualize.plot_isotropy(embedding, ax)

    # 3. t-sne
    plt.subplot(212)
    mangoes.visualize.plot_tsne(embedding)

    plt.show()


def get_demo_corpus():
    """
    Creates a mangoes.Corpus object from an example file

    Returns
    -------
    mangoes.Corpus
    """
    corpus_metadata = os.path.join(os.path.dirname(__file__), "output/.corpus")
    try:
        logging.info("Loading corpus metadata ...")
        corpus = mangoes.Corpus.load_from_metadata(corpus_metadata)
    except FileNotFoundError:
        logging.info("Counting corpus words and sentences ...")
        corpus_path = os.path.join(os.path.dirname(__file__), "data/sample_en_tokenized_750K.txt")
        corpus = mangoes.Corpus(corpus_path, nb_sentences=750000, lower=True)
        corpus.save_metadata(corpus_metadata)
    logging.info("Done. Corpus has {} sentences, {} different words, {} tokens".format(corpus.nb_sentences,
                                                                                       len(corpus.words_count),
                                                                                       corpus.size))
    return corpus


def get_vocabulary(corpus):
    """
    From a corpus, we extract a vocabulary (i.e. a set of words) with the most frequent words.
    Or load a previously created and saved vocabulary

    Parameters
    ----------
    corpus : mangoes.Corpus

    Returns
    -------
    mangoes.Vocabulary
    """

    vocabulary_file_name = "vocabulary_{}_words".format(VOCABULARY_SIZE)
    try:
        logging.info("Loading vocabulary ...")
        vocabulary = mangoes.Vocabulary.load("output", vocabulary_file_name)
    except FileNotFoundError:
        logging.info("Extracting vocabulary from corpus ...")
        vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.truncate(VOCABULARY_SIZE)])
        vocabulary.save(os.path.join(os.path.dirname(__file__), "output"), name=vocabulary_file_name)

    logging.info("Done.\n")
    return vocabulary


if __name__ == "__main__":
    exit(main())
