# -*- coding: utf-8 -*-

import logging

import numpy as np
import pytest

import mangoes.utils.arrays
import mangoes.utils.metrics

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)


@pytest.mark.unittest
def test_rowwise_cosine_similarity():
    x = np.array([1, 0])
    y = np.array([0, 1])

    assert mangoes.utils.metrics.rowwise_cosine_similarity(x, x) == 1
    assert mangoes.utils.metrics.rowwise_cosine_similarity(x, y) == 0

    A = np.array([x, y, x, y])
    B = np.array([x, y, y, x])

    np.testing.assert_array_equal([1., 1., 0., 0.],
                                  mangoes.utils.metrics.rowwise_cosine_similarity(A, B))


@pytest.mark.unittest
def test_non_negative_cosine_similarity():
    x = mangoes.utils.arrays.Matrix.factory(np.array([[0, 1]]))
    y = mangoes.utils.arrays.Matrix.factory(np.array([[1, 0]]))

    assert 1 == mangoes.utils.metrics.pairwise_non_negative_cosine_similarity(x, x)
    assert 1 / 2 == mangoes.utils.metrics.pairwise_non_negative_cosine_similarity(x, y)

    x = [0, 1]
    y = [1, 0]
    A = mangoes.utils.arrays.Matrix.factory(np.array([x, y]))
    B = mangoes.utils.arrays.Matrix.factory(np.array([x, y, x, y]))

    np.testing.assert_array_equal(np.array([[1., 0.5, 1., 0.5],
                                            [0.5, 1., 0.5, 1.]]),
                                  mangoes.utils.metrics.pairwise_non_negative_cosine_similarity(A, B))
