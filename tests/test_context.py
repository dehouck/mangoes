# -*- coding: utf-8 -*-

import pytest
import unittest.mock
import logging

import mangoes.context

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)

# ###########################################################################################
# ### TESTS
sentence = [1, 2, 3, 4, -1, 5]


@pytest.mark.unittest
def test_symmetric_window():
    # default size = 1
    result = mangoes.context.Window()(0, sentence)
    expected = [2]
    assert expected == result

    result = mangoes.context.Window()(2, sentence)
    expected = [2, 4]
    assert expected == result

    result = mangoes.context.Window()(5, sentence)
    expected = []
    assert expected == result

    # window_size = 2
    result = mangoes.context.Window(window_half_size=2)(0, sentence)
    expected = [2, 3]
    assert expected == result

    result = mangoes.context.Window(window_half_size=2)(1, sentence)
    expected = [1, 3, 4]
    assert expected == result

    result = mangoes.context.Window(window_half_size=2)(2, sentence)
    expected = [1, 2, 4]
    assert expected == result

    result = mangoes.context.Window(window_half_size=2)(3, sentence)
    expected = [2, 3, 5]
    assert expected == result

    result = mangoes.context.Window(window_half_size=2)(4, sentence)
    expected = [3, 4, 5]
    assert expected == result

    result = mangoes.context.Window(window_half_size=2)(5, sentence)
    expected = [4]
    assert expected == result


@pytest.mark.unittest
def test_symmetric_window_dirty():
    result = mangoes.context.Window(dirty=True)(0, sentence)
    expected = [2]
    assert expected == result

    result = mangoes.context.Window(dirty=True)(1, sentence)
    expected = [1, 3]
    assert expected == result

    result = mangoes.context.Window(dirty=True)(2, sentence)
    expected = [2, 4]
    assert expected == result

    result = mangoes.context.Window(dirty=True)(3, sentence)
    expected = [3, 5]
    assert expected == result

    result = mangoes.context.Window(dirty=True)(4, sentence)
    expected = [4, 5]
    assert expected == result

    result = mangoes.context.Window(dirty=True)(5, sentence)
    expected = [4]
    assert expected == result


@pytest.mark.unittest
def test_asymmetric_window():
    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2))(0, sentence)
    expected = [2, 3]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2))(1, sentence)
    expected = [1, 3, 4]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2))(2, sentence)
    expected = [2, 4]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2))(3, sentence)
    expected = [3, 5]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2))(4, sentence)
    expected = [4, 5]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2))(5, sentence)
    expected = []
    assert expected == result


@pytest.mark.unittest
def test_asymmetric_window_dirty():
    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2), dirty=True)(0, sentence)
    expected = [2, 3]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2), dirty=True)(1, sentence)
    expected = [1, 3, 4]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2), dirty=True)(2, sentence)
    expected = [2, 4, 5]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2), dirty=True)(3, sentence)
    expected = [3, 5]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2), dirty=True)(4, sentence)
    expected = [4, 5]
    assert expected == result

    result = mangoes.context.Window(symmetric=False, window_half_size=(1, 2), dirty=True)(5, sentence)
    expected = [4]
    assert expected == result


@pytest.mark.unittest
def test_dynamic_window():
    with unittest.mock.patch('random.randint') as mock_random:
        mock_random.side_effect = [1, 2, 3, 1, 2, 3]

        result = mangoes.context.Window(window_half_size=3, dynamic=True)(0, sentence)
        expected = [2]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dynamic=True)(1, sentence)
        expected = [1, 3, 4]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dynamic=True)(2, sentence)
        expected = [1, 2, 4, 5]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dynamic=True)(3, sentence)
        expected = [3]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dynamic=True)(4, sentence)
        expected = [3, 4, 5]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dynamic=True)(5, sentence)
        expected = [3, 4]
        assert expected == result


@pytest.mark.unittest
def test_dynamic_dirty_window():
    with unittest.mock.patch('random.randint') as mock_random:
        mock_random.side_effect = [1, 2, 3, 1, 2, 3]

        result = mangoes.context.Window(window_half_size=3, dirty=True, dynamic=True)(0, sentence)
        expected = [2]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dirty=True, dynamic=True)(1, sentence)
        expected = [1, 3, 4]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dirty=True, dynamic=True)(2, sentence)
        expected = [1, 2, 4, 5]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dirty=True, dynamic=True)(3, sentence)
        expected = [3, 5]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dirty=True, dynamic=True)(4, sentence)
        expected = [3, 4, 5]
        assert expected == result

        result = mangoes.context.Window(window_half_size=3, dirty=True, dynamic=True)(5, sentence)
        expected = [2, 3, 4]
        assert expected == result


@pytest.mark.unittest
def test_dynamic_asymetric_window():
    with unittest.mock.patch('random.randint') as mock_random:
        mock_random.side_effect = [1, 1, 2, 2, 1, 3, 2, 1, 1, 2, 2, 3]

        result = mangoes.context.Window(symmetric=False, window_half_size=(2, 3), dynamic=True)(0, sentence)
        expected = [2]
        assert expected == result

        result = mangoes.context.Window(symmetric=False, window_half_size=(2, 3), dynamic=True)(1, sentence)
        expected = [1, 3, 4]
        assert expected == result

        result = mangoes.context.Window(symmetric=False, window_half_size=(2, 3), dynamic=True)(2, sentence)
        expected = [2, 4, 5]
        assert expected == result

        result = mangoes.context.Window(symmetric=False, window_half_size=(2, 3), dynamic=True)(3, sentence)
        expected = [2, 3]
        assert expected == result

        result = mangoes.context.Window(symmetric=False, window_half_size=(2, 3), dynamic=True)(4, sentence)
        expected = [4, 5]
        assert expected == result

        result = mangoes.context.Window(symmetric=False, window_half_size=(2, 3), dynamic=True)(5, sentence)
        expected = [4]
        assert expected == result
