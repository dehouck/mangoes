# -*- coding: utf-8 -*-

import logging
import os
import pickle
from unittest import mock

import numpy as np
import pytest
from scipy import sparse

import mangoes
import mangoes.counting
import mangoes.utils.arrays
import mangoes.utils.exceptions

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)


# ###########################################################################################
# ### Mocks
class DummyVocabulary:
    def __init__(self, words=[]):
        self.words = words
        self.params = {}
        self.index = self.words.index

    def __getitem__(self, i):
        return self.words[i]


class DummyCooccurrenceCount:
    def __init__(self):
        self.words = DummyVocabulary()
        self.contexts_words = DummyVocabulary()
        self.matrix = np.array([0])
        self.params = {}


def dummy_factory(value):
    return value


# ###########################################################################################
# ### Unit tests

# create_representation function
@pytest.mark.unittest
def test_create_representation_default():
    with mock.patch.object(mangoes.utils.arrays.Matrix, 'factory', side_effect=dummy_factory):
        source = DummyCooccurrenceCount()
        result = mangoes.create_representation(source)
        np.testing.assert_array_equal(source.matrix, result.matrix)


@pytest.mark.unittest
def test_create_representation_with_weighting():
    mock_transformation = mock.Mock()
    mock_transformation.params = {"name": "mock"}
    mock_transformation.return_value = np.array([])

    with mock.patch.object(mangoes.utils.arrays.Matrix, 'factory', side_effect=dummy_factory):
        source = DummyCooccurrenceCount()
        result = mangoes.create_representation(source, weighting=mock_transformation)

        assert mock_transformation.called
        assert 1 == mock_transformation.call_count
        assert {"name": "mock"} == result._params["weighting"]


@pytest.mark.unittest
def test_create_representation_with_weighting_and_reduction():
    mock_transformation = mock.Mock()
    mock_transformation.params = {"name": "mock"}
    mock_transformation.return_value = np.array([])

    with mock.patch.object(mangoes.utils.arrays.Matrix, 'factory', side_effect=dummy_factory):
        source = DummyCooccurrenceCount()
        result = mangoes.create_representation(source,
                                               weighting=mock_transformation,
                                               reduction=mock_transformation)

        assert mock_transformation.called
        assert 2 == mock_transformation.call_count
    assert {"name": "mock"} == result._params["weighting"]
    assert {"name": "mock"} == result._params["reduction"]


# ###########################################################################################
# ### Integration tests

# ## Representation subclasses
@pytest.mark.unitest
def test_get_closest_words_count_based_representation():
    matrix = [[0, 0, 1], [0, 0, 2], [0, 5, 1]]
    vocabulary = DummyVocabulary(['a', 'b', 'c'])
    embeddings = mangoes.CountBasedRepresentation(vocabulary, vocabulary, np.matrix(matrix))
    result = embeddings.get_closest_words("a", 1)

    assert isinstance(result, list)
    assert len(result) == 1
    assert result[0] == ('b', 1)


@pytest.mark.unitest
def test_get_closest_words_embeddings():
    matrix = [[0, 0, 1], [0, 0, 2], [0, 5, 1]]
    embeddings = mangoes.Embeddings(DummyVocabulary(['a', 'b', 'c']), np.matrix(matrix))
    result = embeddings.get_closest_words("a", 1)

    assert isinstance(result, list)
    assert len(result) == 1
    assert result[0] == ('b', 1)


# Persistence
@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_save_load_folder_count_based_representation(save_temp_dir, matrix_type):
    path = os.path.join(str(save_temp_dir), "test_save")

    words_vocabulary = mangoes.Vocabulary(["a", "b", "c"])
    contexts_vocabulary = mangoes.Vocabulary(["d", "e"])
    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    counts = mangoes.CountBasedRepresentation(words_vocabulary, contexts_vocabulary, matrix, hyperparameters={"a": 0})

    # Expected results
    expected = mangoes.CountBasedRepresentation(mangoes.Vocabulary(["a", "b", "c"]),
                                                mangoes.Vocabulary(["d", "e"]),
                                                matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    counts.save(path)
    actual = mangoes.CountBasedRepresentation.load(path)

    # Test
    assert os.path.isdir(path)
    assert expected.words.words == actual.words.words
    assert expected.contexts_words.words == actual.contexts_words.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)
    assert 0 == actual.params["a"]


@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_save_load_archive_count_based_representation(save_temp_dir, matrix_type):
    path = os.path.join(str(save_temp_dir), "test_save_archive.zip")

    words_vocabulary = mangoes.Vocabulary(["a", "b", "c"])
    contexts_vocabulary = mangoes.Vocabulary(["d", "e"])
    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    cooc_count = mangoes.CountBasedRepresentation(words_vocabulary, contexts_vocabulary, matrix,
                                                  hyperparameters={"a": "x"})

    # Expected results
    expected = mangoes.CountBasedRepresentation(mangoes.Vocabulary(["a", "b", "c"]),
                                                mangoes.Vocabulary(["d", "e"]),
                                                matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    cooc_count.save(path)
    actual = mangoes.CountBasedRepresentation.load(path)

    # Test
    assert os.path.isfile(path)
    assert expected.words.words == actual.words.words
    assert expected.contexts_words.words == actual.contexts_words.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)
    assert "x" == actual.params["a"]


@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_save_load_folder_embeddings(save_temp_dir, matrix_type):
    path = os.path.join(str(save_temp_dir), "test_save")

    words = mangoes.Vocabulary(["a", "b", "c"])
    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    embeddings = mangoes.Embeddings(words, matrix)

    # Expected results
    expected = mangoes.Embeddings(mangoes.Vocabulary(["a", "b", "c"]),
                                  matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    embeddings.save(path)
    actual = mangoes.Embeddings.load(path)

    # Test
    assert os.path.isdir(path)
    assert expected.words == actual.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)


@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_save_load_archive_embeddings(save_temp_dir, matrix_type):
    path = os.path.join(str(save_temp_dir), "test_save.zip")

    words = mangoes.Vocabulary(["a", "b", "c"])
    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    embeddings = mangoes.Embeddings(words, matrix)

    # Expected results
    expected = mangoes.Embeddings(mangoes.Vocabulary(["a", "b", "c"]),
                                  matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    embeddings.save(path)
    actual = mangoes.Embeddings.load(path)

    # Test
    assert os.path.isfile(path)
    assert expected.words == actual.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)


@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_save_load_text_files_embeddings(save_temp_dir, matrix_type):
    path = os.path.join(str(save_temp_dir), "test_save.txt")

    words = mangoes.Vocabulary(["a", "b", "c"])
    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    embeddings = mangoes.Embeddings(words, matrix)

    # Expected results
    expected = mangoes.Embeddings(mangoes.Vocabulary(["a", "b", "c"]),
                                  matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    embeddings.save_as_text_file(path)
    actual = mangoes.Embeddings.load_from_text_file(path)

    # Test
    assert os.path.isfile(path)
    assert expected.words == actual.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)


@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_load_from_one_pickle_embeddings(save_temp_dir, matrix_type):
    path = os.path.join(str(save_temp_dir), "test_load_from.pickle")

    words = ["a", "b", "c"]
    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    with open(path, 'wb') as f:
        pickle.dump(words, f)
        pickle.dump(matrix, f)

    # Expected results
    expected = mangoes.Embeddings(mangoes.Vocabulary(["a", "b", "c"]),
                                  matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    actual = mangoes.Embeddings.load_from_pickle_files(path)

    assert expected.words == actual.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)


@pytest.mark.integration
@pytest.mark.parametrize("matrix_type", [np.array, sparse.csr_matrix], ids=["dense", "sparse"])
def test_load_from_two_pickles_embeddings(save_temp_dir, matrix_type):
    words = ["a", "b", "c"]
    vocabulary_file_path = os.path.join(str(save_temp_dir), "test_save_voc.pickle")
    with open(vocabulary_file_path, 'wb') as f:
        pickle.dump(words, f)

    matrix = matrix_type([[0, 5], [3, 0], [6, 9]])
    matrix_file_path = os.path.join(str(save_temp_dir), "test_save_mat.pickle")
    with open(matrix_file_path, 'wb') as f:
        pickle.dump(matrix, f)

    # Expected results
    expected = mangoes.Embeddings(mangoes.Vocabulary(["a", "b", "c"]),
                                  matrix_type([[0, 5], [3, 0], [6, 9]]))

    # Actual results
    actual = mangoes.Embeddings.load_from_pickle_files(matrix_file_path,
                                                       vocabulary_file_path=vocabulary_file_path)

    assert expected.words == actual.words
    try:
        assert np.allclose(expected.matrix, actual.matrix)
    except TypeError:
        assert mangoes.utils.arrays.csrSparseMatrix.allclose(expected.matrix, actual.matrix)


########################
# Exceptions
def test_exception_wrong_separator():
    with pytest.raises(mangoes.utils.exceptions.NotAllowedValue):
        embeddings = mangoes.Embeddings(mangoes.Vocabulary([]), np.matrix([]))
        embeddings.save_as_text_file("here", sep='.')


def test_exception_unknown_transformation():
    source = mock.Mock()
    source.matrix = None
    source.params = {}
    with pytest.raises(mangoes.utils.exceptions.NotAllowedValue):
        mangoes.create_representation(source, reduction="xxx")
