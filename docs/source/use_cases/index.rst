======================
Use cases
======================


Some miscellaneous options and use cases for Mangoes.

.. note:: **Doctest Mode**

   The code in the above examples are written in a *python-console* format.
   If you wish to easily execute these examples in **IPython**, use::

      %doctest_mode

   in the IPython console. You can then simply copy and paste the examples
   directly into IPython without having to worry about removing the **>>>**
   manually.


Creating a Corpus from simple text
----------------------------------

If your corpus consists of a set of text files, preferably tokenized, you can initialize a :class:`.Corpus` object with:

    >>> import mangoes
    >>> corpus = mangoes.Corpus('path/to/corpus')

Once it is initialized, you have access to the number of sentences, the number of words and the frequencies of words in
your corpus:

    >>> print(corpus.size, "sentences")
    >>> print(len(corpus.words_count), "words")
    >>> print(corpus.words_count["a"])
    9450 sentences
    3054 words
    175

The :class:`.Corpus` is also a generator, allowing you to read your corpus sentence by sentence, returning each sentence as a list
of words.

    >>> for sentence in corpus:
    >>>     print(sentence)
    ['anarchism', 'is', 'a', 'political', 'philosophy', 'that', 'advocates', 'self-governed', 'societies', 'based', 'on', 'voluntary', 'institutions.', 'these', 'are', 'often', 'described', 'as', 'stateless', 'societies,', 'although', 'several', 'authors', 'have', 'defined', 'them', 'more', 'specifically', 'as', 'institutions', 'based', 'on', 'non-hierarchical', 'free', 'associations.', 'anarchism', 'considers', 'the', 'state', 'to', 'be', 'undesirable,', 'unnecessary,', 'and', 'harmful,', 'because', 'anarchists', 'generally', 'believe', 'that', 'human', 'beings', 'are', 'capable', 'of', 'managing', 'their', 'own', 'affairs', 'on', 'the', 'basis', 'of', 'creativity,', 'cooperation,', 'and', 'mutual', 'respect,', 'and', 'when', 'making', 'individual', 'decisions', 'they', 'are', 'taking', 'into', 'the', 'account', 'others.']
    ...

.. seealso::
   :class:`.Corpus` documentation and :ref:`parameters for Corpus <corpus-params>`



Creating a Corpus from annotated text
-------------------------------------

If your corpus has been preprocessed and annotated, you have to declare the appropriate Reader to read it.
Currently, the :mod:`mangoes.corpus` module of Mangoes provides 3
readers corresponding to 3 annotation formats:
:class:`mangoes.corpus.XML`, :class:`mangoes.corpus.CONLL` and :class:`mangoes.corpus.BROWN`.

    >>> annotated_corpus = mangoes.Corpus('path/to/XML/annotated/corpus', reader=mangoes.corpus.XML)
    >>> for sentence in annotated_corpus:
    >>>     print(sentence)
    ['word:anarchism/lemma:anarchism/pos:nnp', 'word:is/lemma:be/pos:vbz', 'word:a/lemma:a/pos:dt', 'word:political/lemma:political/pos:jj', 'word:philosophy/lemma:philosophy/pos:nn', 'word:that/lemma:that/pos:wdt', 'word:advocates/lemma:advocate/pos:vbz', 'word:self-governed/lemma:self-governed/pos:jj', 'word:societies/lemma:society/pos:nns', 'word:based/lemma:base/pos:vbn', 'word:on/lemma:on/pos:in', 'word:voluntary/lemma:voluntary/pos:jj', 'word:institutions/lemma:institution/pos:nns', 'word:./lemma:./pos:.']
    ...

You can also filter the available attributes:

    >>> annotated_corpus = mangoes.Corpus('path/to/XML/annotated/corpus',
    >>>                                  reader=mangoes.corpus.XML, filter_attributes=("lemma", "pos"))
    >>> for sentence in annotated_corpus:
    >>>     print(sentence)
    ['lemma:anarchism', 'lemma:be', 'lemma:a', 'lemma:political', 'lemma:philosophy', 'lemma:that', 'lemma:advocate', 'lemma:self-governed', 'lemma:society', 'lemma:base', 'lemma:on', 'lemma:voluntary', 'lemma:institution', 'lemma:.']
    ...

.. note::
    You can also implement and use your own Reader, provided it extends :class:`mangoes.corpus.SentenceGenerator`

Save/load metadata of a Corpus
------------------------------
You can save the metadata associated with a Corpus to load it later.
Saved metadata are:

    >>> corpus.save_metadata(".corpus_metadata")
    >>> corpus = mangoes.Corpus.load_from_metadata(".corpus_metadata")

Creating a Vocabulary from a Corpus
-----------------------------------

To create a :class:`.Vocabulary` with all the words in your :class:`.Corpus`:

    >>> vocabulary = corpus.create_vocabulary()
    >>> print(len(vocabulary))
    3054

You can also use filters when creating a vocabulary from a Corpus.

:mod:`mangoes.corpus` module provides 3 types of filters that can be chained:

* a filter for removing elements defined in a list: :func:`mangoes.corpus.remove_elements`
* 2 filters for removing elements, based on their frequencies: :func:`mangoes.corpus.remove_most_frequent` and :func:`mangoes.corpus.remove_least_frequent`
* a filter for truncating and keeping only a given number of elements: :func:`mangoes.corpus.truncate`

    >>> vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.remove_least_frequent(10),
    >>>                                                mangoes.corpus.truncate(100)])
    >>> print(len(vocabulary))
    100

.. tip::
    To remove punctuation from your Vocabulary:

    >>> import string
    >>> punctuation_filter = mangoes.corpus.remove_elements(string.punctuation)
    >>> vocabulary = corpus.create_vocabulary(filters=[punctuation_filter])

    To remove stopwords:

    >>> import nltk.corpus
    >>> stopwords_filter = mangoes.corpus.remove_elements(nltk.corpus.stopwords.words('english'))
    >>> vocabulary = corpus.create_vocabulary(filters=[stopwords_filter])



.. note::
    You can also write and use your own filters. A filter is a function that takes a :class:`collections.Counter`
    as input and returns a :class:`collections.Counter`. The function should be decorated with
    `@mangoes.utils.decorators.counter_filter`


If the Corpus is annotated, you can also filter the attributes you want to consider in your Vocabulary:

    >>> lemma_vocabulary = annotated_corpus.create_vocabulary(attributes="lemma")
    >>> print(lemma_vocabulary[:5])
    ['lemma:susan', 'lemma:joy', 'lemma:indigenous', 'lemma:possibility', 'lemma:revolt']

    >>> lemma_pos_vocabulary = annotated_corpus.create_vocabulary(attributes=("lemma", "pos"))
    >>> print(lemma_pos_vocabulary[:5])
    ['lemma:area/pos:nns', 'lemma:active/pos:jj', 'lemma:michel/pos:nnp', 'lemma:batchelder/pos:nnp', 'lemma:saornil/pos:nnp']


Count co-occurrences in a Corpus
--------------------------------
Once you have defined your Corpus and your Vocabulary(ies), use the
:mod:`mangoes.counting` module to count how many times the words of your vocabulary(ies) co-occur in the Corpus:

    >>> import mangoes.counting
    >>> counts_matrix = mangoes.counting.count_cooccurrence(corpus, vocabulary, vocabulary)
    >>> print(counts_matrix.shape)
    (100, 100)

This creates an object of class :class:`mangoes.CountBasedRepresentation`.

The `context_definition` parameter lets you define which words are considered to co-occurr. The default value
is a symmetric window of size 1 around each word. 

The module :mod:`mangoes.context` provides the :class:`Window` class that you can use to define window-type
context and parametrize to make it symmetric or not, dynamic or not,
dirty or not: 

    >>> import mangoes.context
    >>> window_5 = mangoes.context.Window(window_half_size=5)
    >>> counts_matrix = mangoes.counting.count_cooccurrence(corpus, vocabulary, vocabulary, context_definition=window_5)


.. note::
    You can also write and use your own contexts. A context definition is a callable class that takes a sentence and
    a position in the sentence and returns the list of the elements of the sentence to consider as the context of
    the word at the given position.


.. seealso::
   :func:`.count_cooccurrence` documentation and :ref:`parameters for counting <counting-params>`


Create a Representation from a co-occurrence matrix
-------------------------------------------------
The :func:`mangoes.create_representation` function turns a counting
matrix into a Representation, applying weighting and/or dimension reduction.

:class:`.Representation` is an abstract class that encapsulates a matrix and a vocabulary.
Mangoes provides 2 implementation of this class:

* the CountBasedRepresentation class for storing sparse matrices with words represented by the rows of the matrix and words used as contexts (the columns of the matrix).
* the Embeddings class for storing low-dimension, dense matrices with words represented by the rows of the matrix.


The :mod:`mangoes.weighting` module provides variants of Pointwise
Mutual Information (PMI) to be used as `weighting` parameter, while
the :mod:`mangoes.reduction` module implements PCA et SVD to be used as `reduction` parameter.

    >>> import mangoes.weighting, mangoes.reduction
    >>> ppmi = mangoes.weighting.PPMI()
    >>> svd = mangoes.reduction.SVD(dimensions=100)
    >>> embedding = mangoes.create_representation(counts_matrix, weighting=ppmi, reduction=svd)


.. note::
    You can also write and use your own transformations functions.
    A transformation definition is a callable class that takes a :class:`mangoes.utils.arrays.Matrix` as input and
    returns a :class:`mangoes.utils.arrays.Matrix`.
    The function should inherit from `.Transformation`


Create Word Embedding with word2vec
-----------------------------------

Mangoes also provides a convenience function for using `gensim's implementation of word2vec
<http://radimrehurek.com/gensim/models/word2vec.html>`_ to produce word embeddings.

    >>> import mangoes.generator.word2vec
    >>> embedding = mangoes.generator.word2vec.create_embeddings(corpus, vocabulary, dimensions=100)

.. seealso::
   :mod:`.word2vec` documentation and :ref:`parameters for counting and embeddings <counting-params>` and
   `Gensim's website <https://radimrehurek.com/gensim/index.html>`_

Evaluate an Embedding on Word Similarity or Analogy Tasks
---------------------------------------------------------
The :mod:`mangoes.evaluate` module currently provides 3 kinds of task to evaluate a representation : Word Similarity, Analogy
and Outlier Detection

    >>> import mangoes.evaluate
    >>> ws_result = mangoes.evaluate.similarity(embedding)
    >>> print(ws_result.summary)
                                                                             pearson        spearman
                                                        Nb questions       (p-value)       (p-value)
    ================================================================================================
    similarity                                              379/6194  0.33(7.19e-09)  0.29(3.70e-07)

    >>> print(ws_result.detail)
                                                                             pearson        spearman
                                                        Nb questions       (p-value)       (p-value)
    ================================================================================================
    similarity                                              379/6194  0.33(7.19e-09)  0.29(3.70e-07)
    ------------------------------------------------------------------------------------------------
        men                                                 198/3000  0.62(3.78e-22)  0.64(7.24e-24)
    ------------------------------------------------------------------------------------------------
        rg65                                                    2/65   1.0(0.00e+00)        1.0(nan)
    ------------------------------------------------------------------------------------------------
        ws353_relatedness                                     52/252  0.47(4.67e-04)  0.43(1.68e-03)
    ------------------------------------------------------------------------------------------------
        wordsim353                                            64/353  0.49(3.21e-05)  0.47(9.71e-05)
    ------------------------------------------------------------------------------------------------
        mturk                                                 17/287  0.57(1.67e-02)  0.62(8.31e-03)
    ------------------------------------------------------------------------------------------------
        rareword                                              9/2034  0.67(4.89e-02)  0.65(5.81e-02)
    ------------------------------------------------------------------------------------------------
        ws353_similarity                                      37/203   0.6(8.22e-05)   0.5(1.73e-03)


    >>> analogy_result = mangoes.evaluate.analogy(embedding)
    >>> print(analogy_result.summary)
                                                                Nb questions      cosadd      cosmul
    ================================================================================================
    analogy                                                        921/27544      56.75%      48.58%

    >>> outlier_result = mangoes.evaluate.outlier_detection(embedding)
    >>> print(outlier_result.summary)
                                                                Nb questions         OPP    accuracy
    ================================================================================================
    outlier_detection                                                 5/2876     100.00%     100.00%



For each of these tasks, some datasets are available in the :mod:`mangoes.dataset` module.

    >>> import mangoes.dataset
    >>> ws353 = mangoes.dataset.load("ws353")
    >>> ws353_result = mangoes.evaluate.similarity(embedding, dataset=ws353)
    >>> print(ws353_result.summary)
                                                                             pearson        spearman
                                                        Nb questions       (p-value)       (p-value)
    ================================================================================================
    wordsim353                                                64/353  0.49(3.21e-05)  0.47(9.71e-05)



Each task can also have specific parameters, like the evaluation metric to use: check their documentation.


Analyse some statistical properties of the Embedding
----------------------------------------------------
The :mod:`mangoes.visualize` module provides some functions to
illustrate some properties of the embedding. 

.. note::
   You have to install `matplotlib <https://matplotlib.org>`_ to use this module


Distances
^^^^^^^^^

:func:`.plot_distances` produces an histogram of the distances between
      each pair of words.

    >>> import matplotlib.pyplot as plt
    >>> import mangoes.visualize
    >>> fig = plt.figure()
    >>> ax = plt.subplot(111, projection='polar') # since distances are angles, we use here a circular histogram
    >>> mangoes.visualize.plot_distances(embedding, ax)
    >>> plt.show()


.. image:: words_distances.png

Isotropy
^^^^^^^^
:func:`plot_isotropy` produces an histogram of the repartition of the values of a partition function:

.. math::
Z_c = \sum_{w}exp(c^\top v_{w})

where :math:`c` is a random vector whose direction is uniformly chosen.

.. topic:: Reference

    * Arora, S., Li, Y., Liang, Y., Ma, T., & Risteski, A. (2015). Rand-walk: A latent variable model approach to word embeddings.


    >>> fig = plt.figure()
    >>> ax = plt.subplot(111)
    >>> mangoes.visualize.plot_isotropy(embedding, ax)
    >>> plt.show()

.. image:: isotropy.png

2D projection
^^^^^^^^^^^^^
:func:`plot_tsne()` creates a 2d projection of the embeddings using t-SNE

    >>> fig = plt.figure()
    >>> mangoes.visualize.plot_tsne(embedding)
    >>> plt.show()
