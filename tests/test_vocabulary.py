# -*- coding: utf-8 -*-
import logging
from collections import Counter

import mangoes
import mangoes.corpus
import mangoes.utils.exceptions
import pytest

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)


# ###########################################################################################
# ### Unit tests

source = {"aa": 0, "bb": 1}
source_list = ["aa", "bb"]


@pytest.mark.unittest
def test_vocabulary_from_dict():
    vocabulary = mangoes.Vocabulary(source)
    assert vocabulary.word_index == source
    assert vocabulary.words == source_list


@pytest.mark.unittest
def test_vocabulary_from_list():
    vocabulary = mangoes.Vocabulary(source_list)
    assert vocabulary.word_index == source
    assert vocabulary.words == source_list


@pytest.mark.unittest
def test_vocabulary_from_vocabulary():
    vocabulary = mangoes.Vocabulary(source)
    actual = mangoes.Vocabulary(vocabulary)
    assert actual.word_index == source
    assert actual.words == source_list


# ###########################################################################################
# ### Integration tests

# Persistence
@pytest.mark.integration
def test_save_load(save_temp_dir):
    vocabulary = mangoes.Vocabulary(['a', 'b', 'c'], language="xx")
    vocabulary.save(str(save_temp_dir), "vocabulary_test")

    loaded_vocabulary = mangoes.Vocabulary.load(str(save_temp_dir), "vocabulary_test")

    assert vocabulary == loaded_vocabulary


########################
# Exceptions
def test_exception_negative_frequencies():
    with pytest.raises(mangoes.utils.exceptions.NotAllowedValue):
        mangoes.corpus.remove_least_frequent(-1, Counter())

    with pytest.raises(mangoes.utils.exceptions.NotAllowedValue):
        mangoes.corpus.remove_most_frequent(-1, Counter())


def test_exception_unsupported_source():
    with pytest.raises(mangoes.utils.exceptions.UnsupportedType):
        mangoes.Vocabulary(1)
