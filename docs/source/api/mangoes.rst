mangoes package
===============

Subpackages
-----------

.. toctree::

    mangoes.generator
    mangoes.utils

Submodules
----------

.. toctree::

   mangoes.base
   mangoes.constants
   mangoes.context
   mangoes.corpus
   mangoes.counting
   mangoes.dataset
   mangoes.evaluate
   mangoes.reduction
   mangoes.visualize
   mangoes.vocabulary
   mangoes.weighting

Module contents
---------------

.. automodule:: mangoes
    :members:
    :undoc-members:
    :show-inheritance:
