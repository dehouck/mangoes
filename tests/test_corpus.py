# -*- coding: utf-8 -*-

import logging
import os.path
from collections import Counter

import pytest

import mangoes
import mangoes.corpus
import mangoes.utils.exceptions

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)


# ###########################################################################################
# ### Mocks

class DummyRawReader:
    def __init__(self, source, lower=False, *args, **kwargs):
        self.source = source
        self.lower = lower

    def sentences(self):
        if self.lower:
            for sentence in self.source:
                yield sentence.lower().split()
        else:
            for sentence in self.source:
                yield sentence.split()


# ###########################################################################################
# ### Unit tests
@pytest.mark.unittest
def test_raw_corpus(raw_source_string, raw_reader_expected):
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader)

    assert 6 == corpus.nb_sentences
    assert 30 == corpus.size
    assert 15 == len(corpus.words_count)
    expected_word_count = {
        'is': 6, 'better': 6, 'than': 6,
        'Beautiful': 1, 'ugly': 1, 'Explicit': 1, 'implicit': 1,
        'Simple': 1, 'complex': 1, 'Complex': 1, 'complicated': 1,
        'Flat': 1, 'nested': 1, 'Sparse': 1, 'dense': 1}

    assert expected_word_count == corpus.words_count

    for expected, sentence in zip(raw_reader_expected, corpus):
        assert expected == sentence


@pytest.mark.unittest
def test_raw_corpus_lower(raw_source_string, raw_reader_expected):
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader, lower=True)

    assert 6 == corpus.nb_sentences
    assert 30 == corpus.size
    assert 14 == len(corpus.words_count)
    expected_word_count = {
        'is': 6, 'better': 6, 'than': 6,
        'beautiful': 1, 'ugly': 1, 'explicit': 1, 'implicit': 1,
        'simple': 1, 'complex': 2, 'complicated': 1,
        'flat': 1, 'nested': 1, 'sparse': 1, 'dense': 1}

    assert expected_word_count == corpus.words_count

    for expected, sentence in zip(raw_reader_expected, corpus):
        assert [token.lower() for token in expected] == sentence


@pytest.mark.unittest
def test_annotated_corpus(DummyAnnotatedReader):
    corpus = mangoes.Corpus(None, reader=DummyAnnotatedReader)

    assert 6 == corpus.nb_sentences
    assert 30 == corpus.size
    assert 15 == len(corpus.words_count)

    expected_word_count = {'word:is/pos:VBZ/lemma:be': 6, 'word:than/pos:IN/lemma:than': 6,
                           'word:better/pos:JJR/lemma:better': 6,
                           'word:Beautiful/pos:JJ/lemma:beautiful': 1, 'word:ugly/pos:JJ/lemma:ugly': 1,
                           'word:Explicit/pos:NNP/lemma:Explicit': 1, 'word:implicit/pos:JJ/lemma:implicit': 1,
                           'word:Simple/pos:NN/lemma:simple': 1, 'word:complex/pos:JJ/lemma:complex': 1,
                           'word:Complex/pos:NN/lemma:complex': 1, 'word:complicated/pos:VBN/lemma:complicate': 1,
                           'word:Flat/pos:NNP/lemma:Flat': 1, 'word:nested/pos:JJ/lemma:nested': 1,
                           'word:Sparse/pos:NNP/lemma:Sparse': 1, 'word:dense/pos:JJ/lemma:dense': 1}

    assert Counter(expected_word_count) == corpus.words_count


@pytest.mark.unittest
def test_lazy_sentences_count_when_words_count(raw_source_string):
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader, lazy=True)
    assert corpus.nb_sentences is None

    corpus.words_count
    assert 6 == corpus.nb_sentences


@pytest.mark.unittest
def test_vocabulary_from_corpus(raw_source_string):
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader)
    vocabulary = corpus.create_vocabulary()
    assert {'is', 'better', 'than', 'Beautiful', 'ugly', 'Explicit', 'implicit', 'Simple', 'complex',
            'Complex', 'complicated', 'Flat', 'nested', 'Sparse', 'dense'} == set(vocabulary.words)


@pytest.mark.unittest
def test_vocabulary_from_corpus_with_dummy_filter(raw_source_string):
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader, lower=True)

    def dummy_filter(counter):
        return {"spam": 0}

    vocabulary = corpus.create_vocabulary(filters=[dummy_filter])
    assert set(vocabulary.word_index) == {'spam'}


@pytest.mark.unittest
def test_vocabulary_of_entities_from_annotated_corpus(DummyAnnotatedReader):
    corpus = mangoes.Corpus(None, reader=DummyAnnotatedReader)

    vocabulary = corpus.create_vocabulary()
    assert {'word:is/pos:VBZ/lemma:be', 'word:better/pos:JJR/lemma:better', 'word:than/pos:IN/lemma:than',
            'word:Beautiful/pos:JJ/lemma:beautiful', 'word:ugly/pos:JJ/lemma:ugly',
            'word:Explicit/pos:NNP/lemma:Explicit', 'word:implicit/pos:JJ/lemma:implicit',
            'word:Simple/pos:NN/lemma:simple', 'word:complex/pos:JJ/lemma:complex',
            'word:Complex/pos:NN/lemma:complex', 'word:complicated/pos:VBN/lemma:complicate',
            'word:Flat/pos:NNP/lemma:Flat', 'word:nested/pos:JJ/lemma:nested',
            'word:Sparse/pos:NNP/lemma:Sparse', 'word:dense/pos:JJ/lemma:dense'} == set(vocabulary.words)

    vocabulary = corpus.create_vocabulary(attributes="word")
    assert {'word:is', 'word:better', 'word:than',
            'word:Beautiful', 'word:ugly',
            'word:Explicit', 'word:implicit',
            'word:Simple', 'word:complex',
            'word:Complex', 'word:complicated',
            'word:Flat', 'word:nested',
            'word:Sparse', 'word:dense'} == set(vocabulary.words)

    vocabulary = corpus.create_vocabulary(attributes=("pos", "lemma"))
    assert {'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
            'pos:JJ/lemma:beautiful', 'pos:JJ/lemma:ugly',
            'pos:NNP/lemma:Explicit', 'pos:JJ/lemma:implicit',
            'pos:NN/lemma:simple', 'pos:JJ/lemma:complex',
            'pos:NN/lemma:complex', 'pos:VBN/lemma:complicate',
            'pos:NNP/lemma:Flat', 'pos:JJ/lemma:nested',
            'pos:NNP/lemma:Sparse', 'pos:JJ/lemma:dense'} == set(vocabulary.words)


# Persistence
@pytest.mark.unittest
def test_save_load_metadata_corpus(tmpdir_factory, raw_source_string):
    # TODO : mock save in file
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader)
    path = str(tmpdir_factory.mktemp('data').join('.corpus'))
    corpus.save_metadata(path)

    assert os.path.isfile(path)

    loaded_corpus = mangoes.Corpus.load_from_metadata(path)

    assert corpus.content == loaded_corpus.content
    assert corpus.nb_sentences == loaded_corpus.nb_sentences
    assert corpus.words_count == loaded_corpus.words_count
    assert corpus.size == loaded_corpus.size


# ###########################################################################################
# ### Integration tests

# ## Integration with SentenceGenerators
# raw text
@pytest.mark.integration
def test_raw_corpus_from_list_of_strings(raw_source_string, raw_reader_expected):
    corpus = mangoes.Corpus(raw_source_string)
    for i, sentence in enumerate(corpus):
        assert raw_reader_expected[i] == sentence


@pytest.mark.integration
def test_raw_corpus_lower_from_list_of_strings(raw_source_string, raw_reader_lower_expected):
    corpus = mangoes.Corpus(raw_source_string, lower=True)
    for i, sentence in enumerate(corpus):
        assert raw_reader_lower_expected[i] == sentence


@pytest.mark.integration
def test_raw_corpus_from_file(raw_source_file, raw_reader_expected):
    corpus = mangoes.Corpus(raw_source_file)
    for i, sentence in enumerate(corpus):
        assert raw_reader_expected[i] == sentence


@pytest.mark.integration
def test_raw_corpus_lower_from_list_of_strings(raw_source_file, raw_reader_lower_expected):
    corpus = mangoes.Corpus(raw_source_file, lower=True)
    for i, sentence in enumerate(corpus):
        assert raw_reader_lower_expected[i] == sentence


@pytest.mark.integration
def test_raw_corpus_from_dir(raw_source_dir, raw_reader_expected):
    corpus = mangoes.Corpus(raw_source_dir)
    for i, sentence in enumerate(corpus):
        assert raw_reader_expected[i] == sentence


# Annotated text : 3 formats : brown, xml or conll
# XML
@pytest.mark.integration
def test_corpus_from_xml_string(xml_source_string, annotated_reader_expected):
    corpus = mangoes.Corpus(xml_source_string, reader=mangoes.corpus.XML)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_corpus_from_xml_file(xml_source_file, annotated_reader_expected):
    corpus = mangoes.Corpus(xml_source_file, reader=mangoes.corpus.XML)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_corpus_from_xml_dir(xml_source_dir, annotated_reader_expected):
    corpus = mangoes.Corpus(xml_source_dir, reader=mangoes.corpus.XML)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


# BROWN
@pytest.mark.integration
def test_corpus_from_brown_string(brown_source_string, annotated_reader_expected):
    corpus = mangoes.Corpus(brown_source_string, reader=mangoes.corpus.BROWN)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_corpus_from_brown_file(brown_source_file, annotated_reader_expected):
    corpus = mangoes.Corpus(brown_source_file, reader=mangoes.corpus.BROWN)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


@pytest.mark.integration
def test_corpus_from_brown_dir(brown_source_dir, annotated_reader_expected):
    corpus = mangoes.Corpus(brown_source_dir, reader=mangoes.corpus.BROWN)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


# CONLL
def test_corpus_from_conll_string(conll_source_string, annotated_reader_expected):
    corpus = mangoes.Corpus(conll_source_string, reader=mangoes.corpus.CONLL)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


def test_corpus_from_conll_file(conll_source_file, annotated_reader_expected):
    corpus = mangoes.Corpus(conll_source_file, reader=mangoes.corpus.CONLL)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


def test_corpus_from_conll_dir(conll_source_dir, annotated_reader_expected):
    corpus = mangoes.Corpus(conll_source_dir, reader=mangoes.corpus.CONLL)
    for i, sentence in enumerate(corpus):
        assert annotated_reader_expected[i] == sentence


# filter attributes
@pytest.mark.integration
def test_corpus_from_xml_and_filter_word_attribute(xml_source_string):
    corpus = mangoes.Corpus(xml_source_string, reader=mangoes.corpus.XML, filter_attributes="word")

    expected = [['word:Beautiful', 'word:is', 'word:better', 'word:than', 'word:ugly'],
                ['word:Explicit', 'word:is', 'word:better', 'word:than', 'word:implicit'],
                ['word:Simple', 'word:is', 'word:better', 'word:than', 'word:complex'],
                ['word:Complex', 'word:is', 'word:better', 'word:than', 'word:complicated'],
                ['word:Flat', 'word:is', 'word:better', 'word:than', 'word:nested'],
                ['word:Sparse', 'word:is', 'word:better', 'word:than', 'word:dense']]
    for i, sentence in enumerate(corpus):
        assert expected[i] == sentence


@pytest.mark.integration
def test_corpus_from_xml_and_filter_pos_lemma_attributes(xml_source_string):
    corpus = mangoes.Corpus(xml_source_string, reader=mangoes.corpus.XML, filter_attributes=("pos", "lemma"))
    expected = [
        ['pos:JJ/lemma:beautiful', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
         'pos:JJ/lemma:ugly'],
        ['pos:NNP/lemma:Explicit', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
         'pos:JJ/lemma:implicit'],
        ['pos:NN/lemma:simple', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
         'pos:JJ/lemma:complex'],
        ['pos:NN/lemma:complex', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than',
         'pos:VBN/lemma:complicate'],
        ['pos:NNP/lemma:Flat', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than', 'pos:JJ/lemma:nested'],
        ['pos:NNP/lemma:Sparse', 'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than', 'pos:JJ/lemma:dense']]
    for i, sentence in enumerate(corpus):
        assert expected[i] == sentence


# ## Integration with vocabulary filters
@pytest.mark.integration
def test_vocabulary_from_corpus_with_filters(raw_source_string):
    corpus = mangoes.Corpus(raw_source_string, reader=DummyRawReader, lower=True)

    min_frequency = 2
    max_frequency = 4
    max_nb = 5

    vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.truncate(max_nb)])
    assert len(vocabulary.words) == 5

    vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.remove_least_frequent(min_frequency)])
    assert set(vocabulary.words) == {'is', 'better', 'than', 'complex'}

    vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.remove_most_frequent(max_frequency),
                                                   mangoes.corpus.remove_least_frequent(min_frequency)])
    assert set(vocabulary.words) == {'complex'}

    vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.remove_least_frequent(min_frequency),
                                                   mangoes.corpus.remove_elements(['is'])])
    assert set(vocabulary.words) == {'better', 'than', 'complex'}


@pytest.mark.integration
def test_vocabulary_of_entities_from_annotated_corpus_with_filters(DummyAnnotatedReader):
    corpus = mangoes.Corpus(None, reader=DummyAnnotatedReader)

    vocabulary = corpus.create_vocabulary(filters=[mangoes.corpus.remove_least_frequent(2)])
    assert {'word:is/pos:VBZ/lemma:be', 'word:better/pos:JJR/lemma:better', 'word:than/pos:IN/lemma:than'} == set(
        vocabulary.words)

    vocabulary = corpus.create_vocabulary(attributes="word", filters=[mangoes.corpus.remove_least_frequent(2)])
    assert {'word:is', 'word:better', 'word:than'} == set(vocabulary.words)

    vocabulary = corpus.create_vocabulary(attributes="lemma", filters=[mangoes.corpus.remove_least_frequent(2)])
    assert {'lemma:be', 'lemma:better', 'lemma:than', 'lemma:complex'} == set(vocabulary.words)

    vocabulary = corpus.create_vocabulary(attributes=("pos", "lemma"), filters=[mangoes.corpus.remove_least_frequent(2)])
    assert {'pos:VBZ/lemma:be', 'pos:JJR/lemma:better', 'pos:IN/lemma:than'} == set(vocabulary.words)

    vocabulary = corpus.create_vocabulary(attributes=("pos", "lemma"),
                                          filters=[mangoes.corpus.remove_least_frequent(2),
                                                   mangoes.corpus.remove_elements("be", attribute="lemma")])
    assert {'pos:JJR/lemma:better', 'pos:IN/lemma:than'} == set(vocabulary.words)


# ## Persistence
@pytest.mark.integration
def test_save_load_metadata_corpus_file(raw_source_file):
    corpus = mangoes.Corpus(raw_source_file, lower=True)
    path = os.path.join(os.path.dirname(raw_source_file), '.corpus')
    corpus.save_metadata(path)

    assert os.path.isfile(path)

    loaded_corpus = mangoes.Corpus.load_from_metadata(path)

    assert corpus.content == loaded_corpus.content
    assert corpus.nb_sentences == loaded_corpus.nb_sentences
    assert corpus.words_count == loaded_corpus.words_count
    assert corpus.size == loaded_corpus.size
    assert corpus.reader.transform.__name__ == loaded_corpus.reader.transform.__name__


@pytest.mark.integration
def test_save_load_metadata_annotated_corpus_file(xml_source_file):
    corpus = mangoes.Corpus(xml_source_file, reader=mangoes.corpus.XML)
    path = os.path.join(os.path.dirname(xml_source_file), '.corpus')
    corpus.save_metadata(path)

    assert os.path.isfile(path)

    loaded_corpus = mangoes.Corpus.load_from_metadata(path)

    assert corpus.content == loaded_corpus.content
    assert corpus.nb_sentences == loaded_corpus.nb_sentences
    assert corpus.words_count == loaded_corpus.words_count
    assert corpus.size == loaded_corpus.size


########################
# Exceptions
@pytest.mark.skip(reason="should raise specific exception")
def test_exception_unknown_format():
    with pytest.raises(AttributeError) as exc:
        corpus = mangoes.Corpus(RAW_SOURCE, reader=print)
        for _ in corpus:
            pass
    assert "'NoneType' object has no attribute 'sentences'" == exc.value.args[0]


def test_exception_wrong_path():
    with pytest.raises(mangoes.utils.exceptions.ResourceNotFound) as exc:
        corpus = mangoes.Corpus("not_existing_dir")
        for _ in corpus:
            pass
    assert "Resource 'not_existing_dir' does not exist" == exc.value.args[0]
