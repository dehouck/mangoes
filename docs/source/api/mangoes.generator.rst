mangoes\.generator package
==========================

Submodules
----------

.. toctree::

   mangoes.generator.word2vec

Module contents
---------------

.. automodule:: mangoes.generator
    :members:
    :undoc-members:
    :show-inheritance:
