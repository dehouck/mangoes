# -*- coding: utf-8 -*-
import logging
import sys
import pytest

from mangoes.corpus import Corpus
from mangoes.vocabulary import Vocabulary
import mangoes.generator.word2vec

import numpy as np

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)

import gensim

gensim_version = tuple(map(int, gensim.__version__.split('.')))
skipif_gensim3 = pytest.mark.skipif(gensim_version >= (3,0,0))


# ###########################################################################################
# ### TESTS
corpus_source = [["aa", "bb", "cc", "dd", "ee"],
                 ["aa", "bb", "cc", "bb", "dd", "ee"],
                 ["aa", "bb", "cc", "dd", "ee"],
                 ["aa", "bb", "cc", "bb", "dd", "ee"],
                 ["aa", "bb", "cc", "dd", "ee"],
                 ["aa", "bb", "cc", "bb", "dd", "ee"]]
corpus = Corpus(corpus_source)
words_vocabulary = Vocabulary(["aa", "bb", "cc", "dd", "ee"])


@pytest.mark.parametrize("window", [1, 2])
@pytest.mark.parametrize("negative", [0, 2])
@pytest.mark.parametrize("subsampling", [None, 4])
def test_word2vec(window, negative, subsampling):
    import gensim.models.word2vec

    # generate with gensim directly
    model = gensim.models.word2vec.Word2Vec(corpus, sg=1, size=5, window=window, negative=negative, sample=subsampling)

    # generate using mangoes
    embeddings = mangoes.generator.word2vec.create_embeddings(corpus, words_vocabulary, 5,
                                                              window_half_size=window,
                                                              negative=negative,
                                                              subsampling=subsampling)

    for word in words_vocabulary:
        np.testing.assert_array_almost_equal(model[word], embeddings[word])


def test_ignored_args():
    import warnings

    with warnings.catch_warnings(record=True) as warnings_recorder:
        warnings.simplefilter("ignore", DeprecationWarning)

        mangoes.generator.word2vec.create_embeddings(corpus, words_vocabulary, 1, sentences=1)
        assert any("sentences" in str(w.message) for w in warnings_recorder)

        mangoes.generator.word2vec.create_embeddings(corpus, words_vocabulary, 1, size=2)
        assert any("size" in str(w.message) for w in warnings_recorder)

        mangoes.generator.word2vec.create_embeddings(corpus, words_vocabulary, 1, min_count=2)
        assert any("min_count" in str(w.message) for w in warnings_recorder)


@skipif_gensim3(reason="Fixed with gensim 3")
def test_exceptions_oov():
    import mangoes.utils.exceptions

    with pytest.raises(mangoes.utils.exceptions.IncompatibleValue) as exc:
        mangoes.generator.word2vec.create_embeddings(corpus, Vocabulary(["zz"]), 5)
    assert "zz" in exc.value.args[0]


def test_exception():
    import mangoes.utils.exceptions

    with pytest.raises(mangoes.utils.exceptions.IncompatibleValue):
        import gensim.models.word2vec
        model = gensim.models.word2vec.Word2Vec(None)
        mangoes.generator.word2vec.create_embeddings_from_gensim_word2vec_model(model)
