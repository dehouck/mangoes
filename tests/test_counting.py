# -*- coding: utf-8 -*-

import logging
import math
import os
import unittest.mock

import numpy as np
import pytest
from scipy import sparse

import mangoes.context
import mangoes.counting
from mangoes.corpus import Corpus
from mangoes.base import CountBasedRepresentation
from mangoes.utils import arrays as arrays_utils
from mangoes.vocabulary import Vocabulary

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)


# ###########################################################################################
# ### Mocks
class DummyVocabulary:
    def __init__(self, words, entity=None):
        self.words = words
        self.word_index = {word: index for index, word in enumerate(words)}
        self.entity = entity

        self.params = {"language": "en"}

    def __len__(self):
        return len(self.words)


class DummyContext:
    # window of one-one around the position
    def __init__(self):
        self.params = {}

    def __call__(self, position, sentence):
        before = [w for w in sentence[max(0, position - 1):position] if w > -1]
        after = [w for w in sentence[position + 1:min(position + 1 + 1, len(sentence))] if w > -1]
        return before + after


# ###########################################################################################
# ### Unit tests

# Corpus : (in conftest.py)
# Beautiful is better than ugly
# Explicit is better than implicit
# Simple is better than complex
# Complex is better than complicated

#                 is   better    than
expected_count = [[1, 0, 0],  # beautiful
                  [0, 0, 1],  # ugly
                  [1, 0, 0],  # simple
                  [1, 0, 1],  # complex
                  [0, 0, 1]]  # complicated


@pytest.mark.unittest
def test_count_cooccurrence(dummy_raw_corpus):
    words = DummyVocabulary(["beautiful", "ugly", "simple", "complex", "complicated"])
    context_words = DummyVocabulary(["is", "better", "than"])

    result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words,
                                                 context_definition=DummyContext())
    assert np.array_equiv(expected_count, result.matrix.toarray())

    result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words,
                                                 context_definition=DummyContext(),
                                                 nb_workers=2)
    assert np.array_equiv(expected_count, result.matrix.toarray())


@pytest.mark.unittest
def test_cooccurrence_word_word(dummy_annotated_corpus):
    words = DummyVocabulary(["word:" + w for w in ["beautiful", "ugly", "simple", "complex", "complicated"]],
                            entity="word")
    context_words = DummyVocabulary(["word:" + w for w in ["is", "better", "than"]],
                                    entity="word")

    result = mangoes.counting.count_cooccurrence(dummy_annotated_corpus, words, context_words,
                                                 context_definition=DummyContext())

    assert np.array_equiv(expected_count, result.matrix.toarray())


@pytest.mark.unittest
def test_cooccurrence_word_lemma(dummy_annotated_corpus):
    words = DummyVocabulary(["word:beautiful", "word:ugly", "word:simple", "word:complex", "word:complicated"],
                            entity="word")
    context_words = DummyVocabulary(["lemma:be", "lemma:better", "lemma:than"], entity="lemma")

    result = mangoes.counting.count_cooccurrence(dummy_annotated_corpus, words, context_words,
                                                 context_definition=DummyContext())

    assert np.array_equiv(expected_count, result.matrix.toarray())


@pytest.mark.unittest
def test_cooccurrence_token_lemma(dummy_annotated_corpus):
    words = DummyVocabulary(['word:beautiful/pos:jj/lemma:beautiful',
                             'word:ugly/pos:jj/lemma:ugly',
                             'word:simple/pos:nn/lemma:simple',
                             'word:complex/pos:jj/lemma:complex',
                             'word:complex/pos:nn/lemma:complex',
                             'word:complicated/pos:vbn/lemma:complicate'],
                            entity=("word", "pos", "lemma"))
    context_words = DummyVocabulary(["lemma:be", "lemma:better", "lemma:than"], entity="lemma")

    #                     is   better    than
    expected_count_alt = [[1, 0, 0],  # beautiful
                          [0, 0, 1],  # ugly
                          [1, 0, 0],  # simple
                          [0, 0, 1],  # complex/JJ
                          [1, 0, 0],  # complex/NN
                          [0, 0, 1]]  # complicated

    result = mangoes.counting.count_cooccurrence(dummy_annotated_corpus, words, context_words,
                                                 context_definition=DummyContext())

    assert np.array_equiv(expected_count_alt, result.matrix.toarray())


# parameters
@pytest.mark.unittest
def test_create_subsampler(dummy_raw_corpus):
    # frequencies of 'is', 'better' and 'than' = 6/30 = 0.2
    # frequencies of 'complex' = 2/30 = 0.0666
    # frequencies of others = 1/30

    # default : threshold = 10**-5
    result = mangoes.counting.create_subsampler(dummy_raw_corpus)

    assert dummy_raw_corpus.words_count.keys() == result.keys()

    assert 1 - math.sqrt(10 ** -5 / (6 / 30)) == result["is"] == result["better"] == result["than"]
    assert 1 - math.sqrt(10 ** -5 / (2 / 30)) == result["complex"]
    assert 1 - math.sqrt(10 ** -5 / (1 / 30)) == result["beautiful"]

    threshold = 0.05
    result = mangoes.counting.create_subsampler(dummy_raw_corpus, threshold=threshold)

    assert {"is", "better", "than", "complex"} == result.keys()

    assert 1 - math.sqrt(threshold / (6 / 30)) == result["is"] == result["better"] == result["than"]
    assert 1 - math.sqrt(threshold / (2 / 30)) == result["complex"]

    threshold = 0.125
    result = mangoes.counting.create_subsampler(dummy_raw_corpus, threshold=threshold)

    assert {"is", "better", "than"} == result.keys()

    expected_probability = 1 - math.sqrt(threshold / 0.2)
    assert expected_probability == result["is"] == result["better"] == result["than"]

    threshold = 0.5
    result = mangoes.counting.create_subsampler(dummy_raw_corpus, threshold=threshold)

    assert {} == result


@pytest.mark.unittest
def test_subsample():
    sentence = ["beautiful", "is", "better", "than", "ugly"]
    expected = [None, "is", "better", "than", "ugly"]

    subsampler = {"beautiful": 0.5, "is": 0.3}

    with unittest.mock.patch('random.random', return_value=0.4):
        assert expected == mangoes.counting._subsample(sentence, subsampler)


@pytest.mark.unittest
def test_words_to_indices():
    sentence = ["a", "b", "c", "a", "b"]
    word2index = {"a": 1, "b": 2}

    assert [1, 2, -1, 1, 2] == mangoes.counting._words_to_indices(sentence, word2index)


# ###########################################################################################
# ### Integration tests

# ## Integration with Corpus and Vocabulary

# raw text
@pytest.mark.integration
def test_count_cooccurrence_raw_text_from_string(raw_source_string):
    corpus = Corpus(raw_source_string, lower=True)
    words = Vocabulary(["beautiful", "ugly", "simple", "complex", "complicated"])
    context_words = Vocabulary(["is", "better", "than"])

    result = mangoes.counting.count_cooccurrence(corpus, words, context_words)

    assert np.array_equiv(expected_count, result.matrix.toarray())


@pytest.mark.integration
def test_count_cooccurrence_raw_text_from_file(raw_source_file):
    corpus = Corpus(raw_source_file, lower=True)
    words = Vocabulary(["beautiful", "ugly", "simple", "complex", "complicated"])
    context_words = Vocabulary(["is", "better", "than"])

    result = mangoes.counting.count_cooccurrence(corpus, words, context_words)

    assert np.array_equiv(result.matrix.toarray(), expected_count)


# annotated text
@pytest.mark.integration
def test_cooccurrence_word_word_from_xml_string(xml_source_string):
    corpus = Corpus(xml_source_string, reader=mangoes.corpus.XML, lower=True)
    words = Vocabulary(["word:" + w for w in ["beautiful", "ugly", "simple", "complex", "complicated"]],
                       entity="word")
    context_words = Vocabulary(["word:" + w for w in ["is", "better", "than"]],
                               entity="word")

    result = mangoes.counting.count_cooccurrence(corpus, words, context_words)

    assert np.array_equiv(expected_count, result.matrix.toarray())


# ## Integration with contexts
@pytest.mark.integration
def test_count_cooccurrence_window_2_2(dummy_raw_corpus):
    words = DummyVocabulary(["beautiful", "ugly", "simple", "complex", "complicated"])
    context_words = DummyVocabulary(["is", "better", "than"])
    symmetric_window_2_2 = mangoes.context.Window(window_half_size=2)

    result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words,
                                                 context_definition=symmetric_window_2_2)

    #                     is   better    than
    expected_count_alt = [[1, 1, 0],  # beautiful
                          [0, 1, 1],  # ugly
                          [1, 1, 0],  # simple
                          [1, 2, 1],  # complex
                          [0, 1, 1]]  # complicated

    assert np.array_equiv(expected_count_alt, result.matrix.toarray())


@pytest.mark.integration
def test_count_cooccurrence_window_1_1_dirty(dummy_raw_corpus):
    words = DummyVocabulary(["beautiful", "simple", "complex"])
    context_words = DummyVocabulary(["ugly", "complex", "complicated"])
    symmetric_window_dirty = mangoes.context.Window(window_half_size=1, dirty=True)

    result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words,
                                                 context_definition=symmetric_window_dirty)

    #                     ugly   complex  complicated
    expected_count_alt = [[1, 0, 0],  # beautiful
                          [0, 1, 0],  # simple
                          [0, 0, 1]]  # complex

    assert np.array_equiv(expected_count_alt, result.matrix.toarray())


@pytest.mark.integration
def test_count_cooccurrence_dynamic_window(dummy_raw_corpus):
    words = DummyVocabulary(["beautiful", "ugly", "simple", "complex", "complicated"])
    context_words = DummyVocabulary(["is", "better", "than"])
    dynamic_window = mangoes.context.Window(window_half_size=3, dynamic=True)

    result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words,
                                                 context_definition=dynamic_window)

    #                     is   better    than
    expected_count_alt = [[1, 1, 1],  # beautiful
                          [1, 1, 1],  # ugly
                          [1, 1, 1],  # simple
                          [2, 2, 2],  # complex
                          [1, 1, 1]]  # complicated

    # with dynamic window, at least one count should be lower than "expected"
    assert any(result.matrix.toarray().flatten() < np.array(expected_count_alt).flatten())


# ## Integration with subsampling
@pytest.mark.integration
def test_count_cooccurrence_subsampling(dummy_raw_corpus):
    words = DummyVocabulary(["beautiful", "ugly", "simple", "complex", "complicated"])
    context_words = DummyVocabulary(["is", "better", "than"])

    # subsampling threshold == 1 -> no subsampling
    result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words, subsampling=1)
    assert np.array_equiv(expected_count, result.matrix.toarray())

    # subsampling threshold == 10**-5, frequencies > 0,03333 -> p ~ 1 for each word
    with unittest.mock.patch('random.random', return_value=0.5):
        result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words, subsampling=True)
        np.testing.assert_array_equal(np.zeros(shape=(5, 3)), result.matrix.toarray())

    # subsampling threshold == 0.125 -> p = 0.2 for 'is', 'better' and 'than'
    with unittest.mock.patch('random.random') as mock_random:
        mock_random.side_effect = [0.1, 0.3] * 9
        result = mangoes.counting.count_cooccurrence(dummy_raw_corpus, words, context_words, subsampling=0.125)

        # Beautiful is      better than ugly
        #           0.1     0.3     0.1
        # Explicit  is      better than implicit
        #           0.3     0.1     0.3
        # Simple    is      better than complex
        #           0.1     0.3     0.1
        # Complex   is      better than complicated
        #           0.3     0.1     0.3

        #                     is   better    than
        expected_count_alt = [[0, 0, 0],  # beautiful
                              [0, 0, 0],  # ugly
                              [0, 0, 0],  # simple
                              [1, 0, 0],  # complex
                              [0, 0, 1]]  # complicated

        assert np.array_equiv(expected_count_alt, result.matrix.toarray())


########################
# Exceptions

def test_exception_no_vocabulary(save_temp_dir):
    with pytest.raises(mangoes.utils.exceptions.RequiredValue):
        corpus = mangoes.Corpus(save_temp_dir, lazy=True)
        mangoes.counting.count_cooccurrence(corpus, words_vocabulary=None, contexts_vocabulary=None)
